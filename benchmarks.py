import argparse
import numpy as np
import os
from SIS_model import InfectiousDiseaseModel
from iPI_solvers import policy_iteration, Rich_PI, SD_PI_sparse, MinRes_PI, GMRES_PI
import time
import json

parser = argparse.ArgumentParser(description='benchmarking iPI')
parser.add_argument('-nt', default = 100, help='number of transitions', type=int)
parser.add_argument('-N', default = 999, help='population size', type=int)
parser.add_argument('-gamma', default = 0.9, help='discount factor', type=float)
parser.add_argument('-alpha', default = 0.01, help='forcing sequence parameter', type=float)
parser.add_argument('-nu', default = 1, help='step-size for Rich', type=float)

args = parser.parse_args()

# parameter
population_size = int(args.N)
num_transitions = int(args.nt) # max. number of non-zero entries in transition probability matrix per row

psi_hygiene = [0.25, 0.125, 0.08, 0.05, 0.03]
cf_hygiene = [0, 1, 5, 6, 9]
cq_hygiene = [1, 0.7, 0.5, 0.4, 0.05]

lambda_social_distancing = np.array([0.2, 0.16, 0.1, 0.01]) * population_size
cf_social_distancing = [0, 1, 10, 30]
cq_social_distancing = [1, 0.9, 0.5, 0.1]

weights = [1, 0.1, 2] # [wf, wq, wh]

visualize_ = True
# create model
idm = InfectiousDiseaseModel(population_size, num_transitions, cf_hygiene, cf_social_distancing, cq_hygiene, cq_social_distancing, psi_hygiene, lambda_social_distancing, weights)


#check if model exists already
f_name = "SISmodel_{}_{}.pkl".format(population_size, num_transitions)
directory = "./SIS_models"

#or create it
if os.path.exists(os.path.join(directory, f_name)):
    print("loading data....")
    with open(os.path.join(directory, f_name), 'rb') as file:
        idm.load(os.path.join(directory, f_name))    

    file.close()

else:
    print("creating the model data...")
    #generate the transition probability matrices and costs
    idm.generate_transition_probabilities()
    idm.generate_stage_costs()

    #save the data
    idm.save()

#visualize transition probabilities
if visualize_:
    idm.visualize_transition_probabilities_sparsitystructure()

np.random.seed(0)

discount_fact = float(args.gamma)
V_0 = np.random.rand(idm.num_states)

folder_ = "results"

if not os.path.isfile(os.path.join(folder_, "PI_results_{}_{}_{}.json".format(discount_fact, population_size, num_transitions))):

    print("PI...")
    start = time.time()
    V_star, policy, iters = policy_iteration(idm, V_0, gamma=discount_fact)
    tot_time = time.time() - start
    
    #save data in json format
    res_PI = {"time": tot_time, "iters": iters}

    with open(os.path.join(folder_, "PI_results_{}_{}_{}.json".format(discount_fact, population_size, num_transitions)), "w") as file:

        json.dump(res_PI, file)

    file.close()

    info = {"Vstar": V_star.tolist(), "gamma": discount_fact, "N": population_size, "transitions": num_transitions, "weights": weights}
    
    with open(os.path.join(folder_, "info_{}_{}_{}.json".format(discount_fact, population_size, num_transitions)), "w") as file:

        json.dump(info, file)

    file.close()

else:
    #open file and extract Vstar
    with open(os.path.join(folder_, "info_{}_{}_{}.json".format(discount_fact, population_size, num_transitions)), "r") as file:
        info = json.load(file)
    file.close()

    V_star = np.asarray(info["Vstar"])

#parameters for iPI and Rich method
alpha_ = float(args.alpha)
nu_ = float(args.nu)

time_limit_ = 500
max_inneriter_ = 100

if not os.path.isfile(os.path.join(folder_, "Rich_results_{}_{}_{}_{}_{}.json".format(discount_fact, population_size, num_transitions, nu_, alpha_))):
    
    print("iPI-Rich...")
        
    start = time.time()
    V_RichPI, policy_RichPI, iters_RichPI = Rich_PI(idm, V_0, V_star, gamma=discount_fact, nu = nu_, alpha = alpha_, max_inneriter=max_inneriter_, time_limit=time_limit_, eps=1e-6)
    tot_time = time.time() - start
    res_Rich = {"time": tot_time, "iters": iters_RichPI}

    with open(os.path.join(folder_, "Rich_results_{}_{}_{}_{}_{}.json".format(discount_fact, population_size, num_transitions, nu_, alpha_)), "w") as file:

        json.dump(res_Rich, file)

    file.close()

if not os.path.isfile(os.path.join(folder_,"SD_results_{}_{}_{}_{}.json".format(discount_fact, population_size, num_transitions, alpha_))):

    print("iPI-SD...")
        
    start = time.time()
    V_SDPI, policy_SDPI, iters_SDPI = SD_PI_sparse(idm, V_0, V_star, gamma=discount_fact, max_inneriter=max_inneriter_, time_limit=time_limit_, eps=1e-6)
    tot_time = time.time() - start
    res_SDPI = {"time": tot_time, "iters": iters_SDPI}

    with open(os.path.join(folder_,"SD_results_{}_{}_{}_{}.json".format(discount_fact, population_size, num_transitions, alpha_)), "w") as file:

        json.dump(res_SDPI, file)

    file.close()

if not os.path.isfile(os.path.join(folder_,"MinRes_results_{}_{}_{}_{}.json".format(discount_fact, population_size, num_transitions, alpha_))):

    print("iPI-MinRes...")
        
    start = time.time()
    V_MinRes, policy_MinRes, iters_MinRes = MinRes_PI(idm, V_0, V_star, gamma=discount_fact, debug = False, max_inneriter=max_inneriter_, time_limit=time_limit_, eps=1e-6)
    tot_time = time.time() - start
    res_MinRes = {"time": tot_time, "iters": iters_MinRes}

    with open(os.path.join(folder_,"MinRes_results_{}_{}_{}_{}.json".format(discount_fact, population_size, num_transitions, alpha_)), "w") as file:

        json.dump(res_MinRes, file)

    file.close()

if not os.path.isfile(os.path.join(folder_,"GMRES_results_{}_{}_{}_{}.json".format(discount_fact, population_size, num_transitions, alpha_))):

    print("iPI-GMRES...")

    start = time.time()
    V_GMRES, policy_GMRES, iters_GMRES = GMRES_PI(idm, V_0, V_star, gamma=discount_fact, alpha = alpha_, time_limit=time_limit_, eps=1e-6)
    tot_time = time.time() - start
    res_GMRES = {"time": tot_time, "iters": iters_GMRES}

    with open(os.path.join(folder_,"GMRES_results_{}_{}_{}_{}.json".format(discount_fact, population_size, num_transitions, alpha_)), "w") as file:

        json.dump(res_GMRES, file)

    file.close()




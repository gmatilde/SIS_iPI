import numpy as np
import scipy
import time
from scipy.stats import binom # to calculate binomial distribution for transition probabilities
from scipy.sparse import csr_matrix, vstack
from scipy.sparse.linalg import spsolve


def extractGreedyPolicy(mdp, V, gamma = 0.99):
    costs = np.empty((mdp.num_states, mdp.num_actions))
    policy = np.zeros(mdp.num_states, dtype=int)
    for state in range(mdp.num_states):
        for action in range(mdp.num_actions):
            costs[state, action] = mdp.stage_cost_matrix[action, state] + gamma * mdp.transition_probability_matrices[action][state, :].dot(V)
    policy[:] = np.argmin(costs, axis=1)
    return policy

def constructFromPolicy(mdp, policy):
    # Collect the rows from each action's transition matrix corresponding to the chosen policy
    rows = [mdp.transition_probability_matrices[policy[state]][state, :] for state in range(mdp.num_states)]
    # Stack these rows vertically to form a single CSR matrix
    P = vstack(rows, format='csr')
    # Calculate the stage cost vector
    g = np.array([mdp.stage_cost_matrix[policy[state], state] for state in range(mdp.num_states)])
    
    return P, g

def policy_iteration(mdp, V_0, gamma=0.99, max_iter=1000, eps=1e-8):

    V = np.copy(V_0)
    
    iters = [V.tolist()]
    
    #extract greedy policy from initial iterate
    policy = extractGreedyPolicy(mdp, V, gamma)

    for i in range(max_iter):
    
        P, g = constructFromPolicy(mdp, policy)
    
        jacobian = scipy.sparse.identity(mdp.num_states, format='csr') - gamma * P
    
        V_new = spsolve(jacobian, g)
    
        policy = extractGreedyPolicy(mdp, V_new, gamma)
    
        norm = np.linalg.norm(V_new - V, ord=np.inf)
        
        if norm == 0:
            break
        V = V_new

        iters.append(V.tolist())
        
    return V, policy, iters


def Rich_PI(mdp, V_0, V_star, gamma=0.99, alpha=0.1, nu=1, max_iter=1000, eps=1e-8, max_inneriter=1000, time_limit=3600):
    
    start = time.time()

    V = np.copy(V_0)
    
    iters = [V.tolist()]
    
    policy = extractGreedyPolicy(mdp, V, gamma)

    for i in range(max_iter):

        inner_iter = 0

        P, g = constructFromPolicy(mdp, policy)
        
        jacobian = scipy.sparse.identity(mdp.num_states, format='csr') - gamma * P

        norm_old = np.linalg.norm(g - jacobian.dot(V), np.inf)

        norm = 10**3*norm_old

        while (norm > alpha*norm_old) and (inner_iter < max_inneriter):
        
            V = (1-(1/nu))*V + (1/nu)*(g + gamma*P.dot(V))
            norm = np.linalg.norm(g - jacobian.dot(V), np.inf)
            inner_iter += 1
        
        policy = extractGreedyPolicy(mdp, V, gamma)

        norm_old = np.copy(norm)

        gap_iters = np.linalg.norm(V - V_star, np.inf) 
        
        iters.append(V.tolist())

        if gap_iters < eps or (time.time() - start)>time_limit:
            break
        
    return V, policy, iters


def SD_PI_sparse(mdp, V_0, V_star, gamma=0.99, alpha=0.01, max_iter=1000, max_inneriter=1000, time_limit=3600, eps=1e-8):
    
    start = time.time()
    
    V = np.copy(V_0)
    
    iters = [V.tolist()]
    
    policy = extractGreedyPolicy(mdp, V, gamma)

    for i in range(max_iter):

        inner_iter = 0

        P, g = constructFromPolicy(mdp, policy)
        
        jacobian = scipy.sparse.identity(mdp.num_states, format='csr') - gamma * P
        jacobian_T = jacobian.transpose()
        residual = g - jacobian.dot(V)

        norm_old = np.linalg.norm(residual, np.inf)

        norm = 10**3*norm_old

        while (norm > alpha*norm_old) and (inner_iter < max_inneriter):

            grad = jacobian_T.dot(-residual)
            Jgrad = jacobian.dot(grad)
            lmbd_star = np.dot(residual.transpose(), -Jgrad)/np.dot(Jgrad.transpose(), Jgrad)
        
            V += - lmbd_star.item()*grad
            
            residual = g - jacobian.dot(V)
            
            norm = np.linalg.norm(residual, np.inf)

            inner_iter += 1

        policy = extractGreedyPolicy(mdp, V, gamma)

        gap_iters = np.linalg.norm(V - V_star, np.inf) 
        
        iters.append(V.tolist())

        if (gap_iters < eps) or (time.time() - start)>time_limit:
            break
        
    return V, policy, iters


def MinRes_PI(mdp, V_0, V_star, gamma=0.99, alpha=0.01, max_iter=1000, eps=1e-8, time_limit=3600, max_inneriter = 1000, debug=True):
    
    start = time.time()

    V = np.copy(V_0)
    
    iters = [V.tolist()]
    
    policy = extractGreedyPolicy(mdp, V, gamma)

    for i in range(max_iter):

        P, g = constructFromPolicy(mdp, policy)
        
        jacobian = scipy.sparse.identity(mdp.num_states, format='csr') - gamma * P
        residual = g - jacobian.dot(V)

        norm_old = np.linalg.norm(residual, np.inf)

        norm = 10**3*norm_old

        inner_iter = 0

        if debug:
            
            jacobian_S = 0.5*(jacobian + jacobian.transpose()).todense()
            eigs, _ = scipy.linalg.eigh(jacobian_S)

            if (eigs<=0).sum()>0:
                print("not PSD!")    

        while (norm > alpha*norm_old) and (inner_iter<max_inneriter):

            Jres = jacobian.dot(residual)
            lmbd_star = np.dot(Jres.transpose(), residual)/np.dot(Jres.transpose(), Jres)
        
            V += lmbd_star.item()*residual
            
            residual = g - jacobian.dot(V)
            
            norm = np.linalg.norm(residual, np.inf)

            inner_iter += 1

        policy = extractGreedyPolicy(mdp, V, gamma)

        gap_iters = np.linalg.norm(V - V_star, np.inf) 
        
        iters.append(V.tolist())

        if (gap_iters < eps) or (time.time() - start)>time_limit:
            break
        
    return V, policy, iters

class Callback:
    def __init__(self, tol):
        self._tol = tol
        
    def __call__(self, pr_norm):

        if pr_norm < self._tol:
            return True
        else:
            return False

def GMRES_PI(mdp, V_0, V_star, gamma=0.99, alpha=0.01, max_iter=1000, time_limit=3600, eps=1e-8):
    
    start = time.time()

    V = np.copy(V_0)
    
    iters = [V.tolist()]
    
    policy = extractGreedyPolicy(mdp, V, gamma)

    for i in range(max_iter):

        P, g = constructFromPolicy(mdp, policy)
        
        jacobian = scipy.sparse.identity(mdp.num_states, format='csr') - gamma * P
        
        RHS = alpha * np.linalg.norm(g - jacobian.dot(V), np.inf)/np.linalg.norm(g)

        #call to GMRES
        V, _ = scipy.sparse.linalg.gmres(jacobian, g, x0=V, tol=RHS, callback=Callback(RHS), callback_type='pr_norm', restart=100)

        policy = extractGreedyPolicy(mdp, V, gamma)

        gap_iters = np.linalg.norm(V - V_star, np.inf) 
        
        iters.append(V.tolist())

        if (gap_iters < eps) or (time.time() - start)>time_limit:
            break
        
    return V, policy, iters

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import matplotlib.cm as cm

import matplotlib.pyplot as plt
plt.rcParams['text.usetex'] = True
import pickle
import scipy
import os
from scipy.stats import binom # to calculate binomial distribution for transition probabilities


class InfectiousDiseaseModel:
    def __init__(self, population_size, num_transitions, cf_a1, cf_a2, cq_a1, cq_a2, psi, lambda_, weights):
        self.population_size = population_size
        self.num_states = population_size + 1 # including state of 0 susceptibles; state = #susceptibles = population_size - #infected
        self.num_actions = 5*4 # 5 hygiene levels, 4 social distancing levels
        self.num_transitions = num_transitions # number of transitions to consider for each action (such that matrix remains sparse and doesn't have lots of entries in the range of 1e-8)
        self.cf_a1 = cf_a1 # financial cost of hygiene measures
        self.cf_a2 = cf_a2 # financial cost of social distancing measures
        self.cq_a1 = cq_a1 # quality of life impact of hygiene measures (1 = full quality of life, 0 = no quality of life)
        self.cq_a2 = cq_a2 # quality of life impact of social distancing measures (1 = full quality of life, 0 = no quality of life)
        self.psi = psi # probability that a susceptible person gets infected upon contact with an infected person
        self.lambda_ = lambda_ # contact rate per day
        self.weights = weights # weights [wf, wq, wh] for cost function (cf, cq, ch)

    def a2ij(self, a): # 1d index to 2d index (i = a1 hygiene, j = a2 social distancing)
        return a % 5, a // 5

    def ch(self, state): # cost of hospitalization (superlinear function of #infected people)
        return (self.population_size - state)**1.1

    def g(self, state, action): # final stage cost
        a1, a2 = self.a2ij(action)
        cf = self.cf_a1[a1] + self.cf_a2[a2]
        cq = self.cq_a1[a1] * self.cq_a2[a2]
        return self.weights[0] * cf - self.weights[1] * cq + self.weights[2] * self.ch(state)

    def q(self, state, action): # overall probability that a susceptible person becomes infected
        a1, a2 = self.a2ij(action)
        beta = 1.0 - 1.0 * state / self.population_size # beta(s) = probability that the next interaction of a random susceptible person is with an infectious person 
        return 1 - np.exp(-beta * self.psi[a1] * self.lambda_[a2])

    def generate_stage_costs(self):
        self.stage_cost_matrix = np.zeros((self.num_states, self.num_actions))
        for state in range(self.num_states):
            for action in range(self.num_actions):
                self.stage_cost_matrix[state, action] = self.g(state, action)
        self.stage_cost_matrix = self.stage_cost_matrix.T # transpose to get right dimensions

    def generate_transition_probabilities(self):
        self.transition_probability_matrices = []

        for action in range(self.num_actions):
            # Lists to store the data and coordinates for the CSR matrix
            data = []
            row = []
            col = []

            for state in range(self.num_states):
                if state == self.population_size:
                    row.append(state)
                    col.append(self.population_size)
                    data.append(1.0)
                    continue

                q_prob = self.q(state, action)
                ev = state * q_prob
                start = int(max(0, ev - self.num_transitions // 2))
                end = int(min(state, ev + self.num_transitions // 2))
                binom_values = binom.pmf(np.arange(start, end + 1), state, q_prob)
                sum_binom = np.sum(binom_values)

                for i in range(start, end + 1):
                    next_state = self.population_size - i
                    prob = binom_values[i - start] / sum_binom
                    row.append(state)
                    col.append(next_state)
                    data.append(prob)

            # Create a CSR matrix for the current action
            matrix = scipy.sparse.csr_matrix((data, (row, col)), shape=(self.num_states, self.num_states))
            self.transition_probability_matrices.append(matrix)


    def visualize_transition_probabilities_sparsitystructure(self):
        cmap = matplotlib.colors.ListedColormap(['white', 'black'])
        fig, axs = plt.subplots(4, 5, figsize=(20, 16))
        for action in range(self.num_actions):
            i, j = self.a2ij(action)
            ax = axs[j, i]
            im = ax.imshow(self.transition_probability_matrices[action].todense()!=0, cmap=cmap, interpolation='nearest')
        
        plt.savefig("SIS_sparsitystructure_{}_{}.png".format(self.population_size, self.num_transitions), dpi = 300)

    def save(self):
        dir = "./SIS_models"

        if not os.path.isdir(dir):
            os.makedirs(dir)

        data = {"P": self.transition_probability_matrices, "g": self.stage_cost_matrix}
        f_name = "SISmodel_{}_{}.pkl".format(self.population_size, self.num_transitions)
        pickle.dump(data, open(os.path.join(dir, f_name),"wb"))

    def load(self, path):

        with open(path, "rb") as file:
            data = pickle.load(file)
        file.close()

        self.transition_probability_matrices = data["P"]
        self.stage_cost_matrix = data["g"]


if __name__ == "__main__":

    import argparse
    import pickle
    import os

    parser = argparse.ArgumentParser(description='benchmarking iPI')
    parser.add_argument('-nt', default = 100, help='number of transitions', type=int)
    parser.add_argument('-N', default = 999, help='population size', type=int)
    parser.add_argument('-gamma', default = 0.9, help='discount factor', type=float)
    parser.add_argument('-alpha', default = 0.01, help='forcing sequence parameter', type=float)
    parser.add_argument('-nu', default = 1, help='step-size for Rich', type=float)

    args = parser.parse_args()

    # parameter
    population_size = int(args.N)
    num_transitions = int(args.nt) # max. number of non-zero entries in transition probability matrix per row

    psi_hygiene = [0.25, 0.125, 0.08, 0.05, 0.03]
    cf_hygiene = [0, 1, 5, 6, 9]
    cq_hygiene = [1, 0.7, 0.5, 0.4, 0.05]

    lambda_social_distancing = np.array([0.2, 0.16, 0.1, 0.01]) * population_size
    cf_social_distancing = [0, 1, 10, 30]
    cq_social_distancing = [1, 0.9, 0.5, 0.1]

    weights = [1, 0.1, 2] # [wf, wq, wh]

    visualize_ = True
    # create model
    idm = InfectiousDiseaseModel(population_size, num_transitions, cf_hygiene, cf_social_distancing, cq_hygiene, cq_social_distancing, psi_hygiene, lambda_social_distancing, weights)


    #check if model exists already
    f_name = "SISmodel_{}_{}.pkl".format(population_size, num_transitions)
    directory = "./SIS_models"

    #or create it
    if os.path.exists(os.path.join(directory, f_name)):
        print("loading data....")
        with open(os.path.join(directory, f_name), 'rb') as file:
            idm.load(os.path.join(directory, f_name))    

        file.close()

    else:
        print("creating the model data...")
        #generate the transition probability matrices and costs
        idm.generate_transition_probabilities()
        idm.generate_stage_costs()

        #save the data
        idm.save()

    #visualize transition probabilities
    if visualize_:
        idm.visualize_transition_probabilities_sparsitystructure()

    from iPI_SOLVE import policy_iteration, Rich_PI, SD_PI_sparse, MinRes_PI, GMRES_PI
    import time
    import json

    np.random.seed(0)

    discount_fact = float(args.gamma)
    V_0 = np.random.rand(idm.num_states)
    
    #if it has not been run before

    if not os.path.isfile("PI_results_{}_{}_{}.json".format(discount_fact, population_size, num_transitions)):
    
        print("PI...")
        start = time.time()
        V_star, policy, iters = policy_iteration(idm, V_0, gamma=discount_fact)
        tot_time = time.time() - start
        
        #save data in json format
        res_PI = {"time": tot_time, "iters": iters}

        with open("PI_results_{}_{}_{}.json".format(discount_fact, population_size, num_transitions), "w") as file:

            json.dump(res_PI, file)

        file.close()

        info = {"Vstar": V_star.tolist(), "gamma": discount_fact, "N": population_size, "transitions": num_transitions, "weights": weights}
        
        with open("info_{}_{}_{}.json".format(discount_fact, population_size, num_transitions), "w") as file:

            json.dump(info, file)

        file.close()
    
    else:
        #open file and extract Vstar
        with open("info_{}_{}_{}.json".format(discount_fact, population_size, num_transitions), "r") as file:
            info = json.load(file)
        file.close()

        V_star = np.asarray(info["Vstar"])

    #parameters for iPI and Rich method
    alpha_ = float(args.alpha)
    nu_ = float(args.nu)

    time_limit_ = 500
    max_inneriter_ = 100

    if not os.path.isfile("Rich_results_{}_{}_{}_{}_{}.json".format(discount_fact, population_size, num_transitions, nu_, alpha_)):
        
        print("iPI-Rich...")
        start = time.time()
        V_RichPI, policy_RichPI, iters_RichPI = Rich_PI(idm, V_0, V_star, gamma=discount_fact, nu = nu_, alpha = alpha_, max_inneriter=max_inneriter_, time_limit=time_limit_, eps=1e-6)
        tot_time = time.time() - start
        res_Rich = {"time": tot_time, "iters": iters_RichPI}

        with open("Rich_results_{}_{}_{}_{}_{}.json".format(discount_fact, population_size, num_transitions, nu_, alpha_), "w") as file:

            json.dump(res_Rich, file)

        file.close()
    
    if not os.path.isfile("SD_results_{}_{}_{}_{}.json".format(discount_fact, population_size, num_transitions, alpha_)):
    
        print("iPI-SD")
        start = time.time()
        V_SDPI, policy_SDPI, iters_SDPI = SD_PI_sparse(idm, V_0, V_star, gamma=discount_fact, max_inneriter=max_inneriter_, time_limit=time_limit_, eps=1e-6)
        tot_time = time.time() - start
        res_SDPI = {"time": tot_time, "iters": iters_SDPI}

        with open("SD_results_{}_{}_{}_{}.json".format(discount_fact, population_size, num_transitions, alpha_), "w") as file:

            json.dump(res_SDPI, file)

        file.close()

    if not os.path.isfile("MinRes_results_{}_{}_{}_{}.json".format(discount_fact, population_size, num_transitions, alpha_)):

        print("iPI-MinRes")
        start = time.time()
        V_MinRes, policy_MinRes, iters_MinRes = MinRes_PI(idm, V_0, V_star, gamma=discount_fact, debug = False, max_inneriter=max_inneriter_, time_limit=time_limit_, eps=1e-6)
        tot_time = time.time() - start
        res_MinRes = {"time": tot_time, "iters": iters_MinRes}

        with open("MinRes_results_{}_{}_{}_{}.json".format(discount_fact, population_size, num_transitions, alpha_), "w") as file:

            json.dump(res_MinRes, file)

        file.close()

    if not os.path.isfile("GMRES_results_{}_{}_{}_{}.json".format(discount_fact, population_size, num_transitions, alpha_)):
    
        print("iPI-GMRES...")
        start = time.time()
        V_GMRES, policy_GMRES, iters_GMRES = GMRES_PI(idm, V_0, V_star, gamma=discount_fact, alpha = alpha_, time_limit=time_limit_, eps=1e-6)
        tot_time = time.time() - start
        res_GMRES = {"time": tot_time, "iters": iters_GMRES}

        with open("GMRES_results_{}_{}_{}_{}.json".format(discount_fact, population_size, num_transitions, alpha_), "w") as file:

            json.dump(res_GMRES, file)

        file.close()



